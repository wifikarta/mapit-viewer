package importer

import java.nio.{BufferUnderflowException, ByteBuffer}
import java.time.format.DateTimeFormatter
import java.time.{Instant, LocalDate, LocalTime, ZoneOffset}

import com.vividsolutions.jts.geom.{Coordinate, GeometryFactory, Point}

import scala.util.control.NonFatal

class MapItReader(buf: ByteBuffer) {
  private var header: Option[MapItFileHeader] = None
  var currentElement: Long = 0
  private var debugLog: Option[Array[Byte]] = None
  val geomFactory = new GeometryFactory()

  def init(): Unit = {
    buf.position(0)
    header = Some(readFileHeader())
    debugLog = Some(readDebugLog())
    assert(buf.position() == 1048576)
  }

  private def readFileHeader(): MapItFileHeader = {
    val raw = Array.ofDim[Byte](1024)
    buf.get(raw)
    // TODO: Read header contents instead of hardcoding!!
    val elementCount = 50
    MapItFileHeader()
  }

  private def readDebugLog(): Array[Byte] = {
    val raw = Array.ofDim[Byte](1047552)
    buf.get(raw)
    raw
  }

  def readChunk(): Option[MapItChunk] = {
    buf.mark() // Record the current position
    val pos = buf.position()
    val len = buf.get()
    val tpe = buf.get()
    if (len == 0) {
      return None // EOF
    }
    val rawContent = Array.ofDim[Byte](len - (tpe match {
      case 1 => 1 // TODO: Remove, len includes type in length for some reason
      case _ => 0
    }))
    buf.get(rawContent)
    val content = new String(rawContent, "ASCII") // TODO: Correct charset?

    val chunk = try {
      tpe match {
        case 0 => parseGPSChunk(content)
        case 1 => parseWifiChunk(content).fold(identity, identity)
        case _ =>
          throw new UnknownChunkType(tpe, pos)
      }
    } catch {
      case ex: BufferUnderflowException =>
        // We're out of file to read, so abort...
        throw ex
      case ex: UnknownChunkType =>
        ex.printStackTrace()
        println("Trying to find next GPS chunk")
        buf.reset()
        var foundChunk = false
        while (!foundChunk) {
          val byte = buf.get()
          if (byte == '$' && buf.get(buf.position() - 3) == 0) {
            foundChunk = true
          }
        }
        return readChunk()
      case NonFatal(ex) =>
        // Just try again..
        ex.printStackTrace()
        return readChunk()
    }
    currentElement += 1
    buf.mark() // "Commit" the read, and advance the read pointer permanently
    Some(chunk)
  }

  private def parseGPSChunk(content: String): GPSChunk = {
    val parts = content.stripLineEnd.split(',')
    val time = LocalTime.from(MapItReader.timeFormatter.parse(parts(1)))
    val latitude = parseCoord(parts(3), 2)
    val longitude = parseCoord(parts(5), 3)
    val point = geomFactory.createPoint(new Coordinate(latitude, longitude))
    val date = LocalDate.from(MapItReader.dateFormatter.parse(parts(9)))
    val instant = date.atTime(time).toInstant(ZoneOffset.UTC)
    // Some genius thought knots were the universal standard of measuring speeds
    // Oh well, at least it's not miles...
    val speed = parts(7) match {
      case "" => 0
      case s => s.toDouble * 1.852
    }
    GPSChunk(instant, speed, point)
  }

  private def parseCoord(raw: String, degLen: Short): Double = {
    raw.substring(0, degLen).toDouble + raw.substring(degLen).toDouble / 60
  }

  private def parseWifiChunk(content: String): Either[NonsenseChunk, WifiChunk] = {
    val parts = content.split(",", 9)
    if (parts.length == 9) {
      Right(WifiChunk(
        index = parts(0).toShort,
        channel = parts(1).toShort,
        rssi = parts(2).toShort,
        securityMode = parts(3).toShort match {
          case 0 => "NONE"
          case 1 => "WEP"
          case 2 => "WPA-PSK"
          case 3 => "WPA-MIXED-PSK"
          case 4 => "WPA2-PSK"
          case 6 => "WPA-802.1x"
          case 7 => "WPA-MIXED-802.1x"
          case 8 => "WPA2-802.1x"
        },
        capabilities = parts(4),
        wpaConfig = parts(5),
        wpsMode = parts(6),
        bssid = parts(7).ensuring(verifyMacAddress _, s"Invalid MAC address: ${parts(7)}"),
        essid = parts(8).replace("\0", "").trim
      ))
    } else {
      Left(NonsenseChunk(content, buf.position()))
    }
  }

  private def verifyMacAddress(mac: String): Boolean = {
    val parts = mac.split(':')
    def isMacCharacter(c: Char) = c == ':' || (c >= '0' && c <= '9') || (c >= 'a' && c <= 'f')
    parts.length == 6 && parts.forall(_.length == 2) && mac.forall(isMacCharacter)
  }
}

object MapItReader {
  val dateFormatter = DateTimeFormatter.ofPattern("ddMMyy")
  val timeFormatter = DateTimeFormatter.ofPattern("HHmmss.SSS")
}

case class MapItFileHeader()

sealed trait MapItChunk

case class GPSChunk(time: Instant, speed: Double, point: Point) extends MapItChunk

case class WifiChunk(index: Short, channel: Short, rssi: Short, securityMode: String, capabilities: String, wpaConfig: String, wpsMode: String, bssid: String, essid: String) extends MapItChunk

case class NonsenseChunk(str: String, pos: Int) extends MapItChunk

case class UnknownChunkType(tpe: Byte, pos: Int) extends Exception(s"Unknown chunk type $tpe at ${Integer.toHexString(pos)}")