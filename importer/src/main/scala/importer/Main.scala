package importer

import java.io.{File, FileInputStream}
import java.nio.channels.FileChannel
import java.sql.Timestamp

import models.MyPgDriver.api._
import models.Tables
import models.Tables.WifiPollSpotRow
import utils.db.SlickUtils

import scala.collection.immutable.Queue
import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.util.control.Breaks

object Main extends App {
  val src = args.toList match {
    case path :: Nil => new File(path)
    case _ =>
      println("Usage: importer <file>")
      sys.exit(1)
  }

  val srcBuf = {
    val srcStream = new FileInputStream(src)
    val srcChannel = srcStream.getChannel
    val srcBuf = srcChannel.map(FileChannel.MapMode.READ_ONLY, 0, src.length())
    srcChannel.close()
    srcStream.close()
    srcBuf
  }

  val db = SlickUtils.getDB
  val reader = new MapItReader(srcBuf)
  reader.init()

  var currentGPSChunk: Option[GPSChunk] = None
  var currentGPSChunks: Queue[GPSChunk] = Queue()
  Breaks.breakable {
    while (true) {
      val chunk = {
        reader.readChunk().getOrElse(Breaks.break)
      }
      (chunk, currentGPSChunk) match {
        case (gps: GPSChunk, _) =>
          currentGPSChunk = Some(gps)
          currentGPSChunks :+= gps
          // Only keep around the last 3 seconds
          currentGPSChunks.dropWhile(_.time.compareTo(gps.time.minusSeconds(3)) < 0)
        case (wifi: WifiChunk, Some(gps)) =>
          val timeTravellingGPSChunk = {
            // Each channel takes ~200ms to scan, but it's all dumped simultaneously, so we need to travel back in time a bit
            val delay = (13 - wifi.channel) * 200
            currentGPSChunks
              .find(_.time.compareTo(gps.time.minusMillis(delay)) >= 0)
              .get
          }

          val ids = Await.result(db.run(
            for {
              // ON CONFLICT DO NOTHING would be ideal, but it doesn't cause the id to be returned
              netId <- sql"""INSERT INTO wifi_network(essid)
                             VALUES(${wifi.essid.replace("\0", "")})
                             ON CONFLICT(essid) DO UPDATE SET essid=wifi_network.essid
                             RETURNING id""".as[Int]
              stationId <- sql"""INSERT INTO wifi_station(bssid, network, encryption)
                                 VALUES(${wifi.bssid}::MACADDR, ${netId(0)}, ${wifi.securityMode.toString}::wifi_encryption_type)
                                 ON CONFLICT(bssid) DO UPDATE SET bssid=wifi_station.bssid
                                 RETURNING id""".as[Int]
              _ <- Tables.WifiPollSpot += WifiPollSpotRow(-1, timeTravellingGPSChunk.point, stationId(0), Timestamp.from(timeTravellingGPSChunk.time), timeTravellingGPSChunk.speed)
            } yield (netId, stationId)
          ), Duration.Inf)
          println((wifi, gps, ids))
        case (nonsense: NonsenseChunk, _) =>
          println(s"Got a garbage chunk at ${Integer.toHexString(nonsense.pos)}: ${nonsense.str}")
        case (_, None) =>
          println("Got another chunk before the first GPS chunk")
      }
    }
  }
}
