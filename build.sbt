import com.typesafe.sbt.packager.archetypes.ServerLoader
import play.sbt.routes.RoutesKeys._

lazy val slickCodegen = taskKey[Unit]("Generates Slick models")

lazy val pkg = taskKey[File]("Build all packages")

lazy val sharedSettings = Seq[Def.Setting[_]](
  version := "1.0-SNAPSHOT",
  maintainer := "Teo Klestrup Röijezon <teo@nullable.se>",
  maintainer in Debian := maintainer.value.replace('ö', 'o'), // Debian doesn't like ÅÄÖ
  scalacOptions ++= Seq("-Xmax-classfile-name", "140"), // Don't break filesystems that have harsher filename limitations, like eCryptfs
  scalaVersion := "2.11.7",
  serverLoading in Debian := ServerLoader.Systemd
)

lazy val root = (project in file("."))
  .aggregate(viewerJVM, viewerJS, importer, driver, tables)
  .settings(
    pkg := {
      val targetDir = target.value / "pkg"
      val viewerPkg = (packageBin in (viewerJVM, Debian)).value
      val importerPkg = (packageBin in (importer, Debian)).value
      IO.createDirectory(targetDir)
      IO.copyFile(viewerPkg, targetDir / viewerPkg.getName)
      IO.copyFile(importerPkg, targetDir / importerPkg.getName)
      targetDir
    }
  )

lazy val viewer = crossProject.in(file("."))
  .settings(
    name := "MapIt-Viewer",
    resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases",
    routesGenerator := InjectedRoutesGenerator,
    libraryDependencies += "com.lihaoyi" %%% "upickle" % "0.3.8"
  )
  .settings(sharedSettings: _*)
  .jvmConfigure(_.enablePlugins(PlayScala, PlayScalaJS, JavaServerAppPackaging, JDebPackaging).dependsOn(driver, tables))
  .jvmSettings(
    libraryDependencies ++= Seq(
      //jdbc,
      cache,
      ws,
      "com.typesafe.play" %% "play-slick" % "1.1.1",
      "com.typesafe.play" %% "play-slick-evolutions" % "1.1.1",
      "org.webjars.npm" % "jquery" % "2.2.0",
      "com.vmunier" %% "play-scalajs-scripts" % "0.4.0",
      specs2 % Test,
      // For the CSS stylesheets
      "org.webjars.npm" % "leaflet" % "0.7.7"
    ),
    pipelineStages += scalaJSProd
  )
  .jsConfigure(_.enablePlugins(ScalaJSPlay))
  .jsSettings(
    libraryDependencies += "org.scala-js" %%% "scalajs-dom" % "0.9.0",
    jsDependencies ++= Seq(
      RuntimeDOM,
      //"org.webjars.npm" % "leaflet" % "0.7.7" / "dist/leaflet.css",
      "org.webjars.npm" % "leaflet" % "0.7.7" / "dist/leaflet.js",
      "org.webjars.npm" % "leaflet-providers" % "1.1.7" / "leaflet-providers.js"
    ),
    persistLauncher := true
  )

lazy val viewerJVM = viewer.jvm.settings(scalaJSProjects := Seq(viewerJS))
lazy val viewerJS = viewer.js

lazy val importer = project
  .settings(
    name := "MapIt-Importer"
  )
  .settings(sharedSettings: _*)
  .enablePlugins(JavaAppPackaging, JDebPackaging)
  .dependsOn(driver, tables)

lazy val driver = project
  .settings(
    name := "MapIt-Slick-Driver",
    libraryDependencies ++= Seq(
      "com.typesafe.slick" %% "slick" % "3.1.1",
      "com.typesafe.slick" %% "slick-codegen" % "3.1.1",
      "com.typesafe.slick" %% "slick-hikaricp" % "3.1.1",
      "org.postgresql" % "postgresql" % "9.4.1207",
      "com.github.tminglei" %% "slick-pg" % "0.11.2",
      "com.github.tminglei" %% "slick-pg_date2" % "0.11.2",
      "com.github.tminglei" %% "slick-pg_jts" % "0.11.2"
    )
  )
  .settings(sharedSettings: _*)

lazy val tables = project
  .dependsOn(driver)
  .settings(
    slickCodegen in Compile := {
      val outFolder = (scalaSource in Compile).value
      runner.value.run("models.MySourceCodeGen", (dependencyClasspath in Runtime).value.files, Array(outFolder.absolutePath), streams.value.log)
    }
  )
  .settings(sharedSettings: _*)

run in root <<= run in(viewerJVM, Compile)
