package models
// AUTO-GENERATED Slick data model
/** Stand-alone Slick data model for immediate use */
object Tables extends {
  val profile = models.MyPgDriver
} with Tables

/** Slick data model trait for extension, choice of backend or usage in the cake pattern. (Make sure to initialize this late.) */
trait Tables {
  val profile: models.MyPgDriver
  import profile.api._
  import slick.model.ForeignKeyAction
  // NOTE: GetResult mappers for plain SQL are only generated for tables where Slick knows how to map the types of all columns.
  import slick.jdbc.{GetResult => GR}

  /** DDL for all tables. Call .create to execute. */
  lazy val schema: profile.SchemaDescription = Array(PlayEvolutions.schema, SpatialRefSys.schema, WifiKnownCredentials.schema, WifiNetwork.schema, WifiPollSpot.schema, WifiStation.schema).reduceLeft(_ ++ _)
  @deprecated("Use .schema instead of .ddl", "3.0")
  def ddl = schema

  /** Entity class storing rows of table PlayEvolutions
   *  @param id Database column id SqlType(int4), PrimaryKey
   *  @param hash Database column hash SqlType(varchar), Length(255,true)
   *  @param appliedAt Database column applied_at SqlType(timestamp)
   *  @param applyScript Database column apply_script SqlType(text), Default(None)
   *  @param revertScript Database column revert_script SqlType(text), Default(None)
   *  @param state Database column state SqlType(varchar), Length(255,true), Default(None)
   *  @param lastProblem Database column last_problem SqlType(text), Default(None) */
  case class PlayEvolutionsRow(id: Int, hash: String, appliedAt: java.sql.Timestamp, applyScript: Option[String] = None, revertScript: Option[String] = None, state: Option[String] = None, lastProblem: Option[String] = None)
  /** GetResult implicit for fetching PlayEvolutionsRow objects using plain SQL queries */
  implicit def GetResultPlayEvolutionsRow(implicit e0: GR[Int], e1: GR[String], e2: GR[java.sql.Timestamp], e3: GR[Option[String]]): GR[PlayEvolutionsRow] = GR{
    prs => import prs._
    PlayEvolutionsRow.tupled((<<[Int], <<[String], <<[java.sql.Timestamp], <<?[String], <<?[String], <<?[String], <<?[String]))
  }
  /** Table description of table play_evolutions. Objects of this class serve as prototypes for rows in queries. */
  class PlayEvolutions(_tableTag: Tag) extends Table[PlayEvolutionsRow](_tableTag, "play_evolutions") {
    def * = (id, hash, appliedAt, applyScript, revertScript, state, lastProblem) <> (PlayEvolutionsRow.tupled, PlayEvolutionsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(hash), Rep.Some(appliedAt), applyScript, revertScript, state, lastProblem).shaped.<>({r=>import r._; _1.map(_=> PlayEvolutionsRow.tupled((_1.get, _2.get, _3.get, _4, _5, _6, _7)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(int4), PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.PrimaryKey)
    /** Database column hash SqlType(varchar), Length(255,true) */
    val hash: Rep[String] = column[String]("hash", O.Length(255,varying=true))
    /** Database column applied_at SqlType(timestamp) */
    val appliedAt: Rep[java.sql.Timestamp] = column[java.sql.Timestamp]("applied_at")
    /** Database column apply_script SqlType(text), Default(None) */
    val applyScript: Rep[Option[String]] = column[Option[String]]("apply_script", O.Default(None))
    /** Database column revert_script SqlType(text), Default(None) */
    val revertScript: Rep[Option[String]] = column[Option[String]]("revert_script", O.Default(None))
    /** Database column state SqlType(varchar), Length(255,true), Default(None) */
    val state: Rep[Option[String]] = column[Option[String]]("state", O.Length(255,varying=true), O.Default(None))
    /** Database column last_problem SqlType(text), Default(None) */
    val lastProblem: Rep[Option[String]] = column[Option[String]]("last_problem", O.Default(None))
  }
  /** Collection-like TableQuery object for table PlayEvolutions */
  lazy val PlayEvolutions = new TableQuery(tag => new PlayEvolutions(tag))

  /** Entity class storing rows of table SpatialRefSys
   *  @param srid Database column srid SqlType(int4), PrimaryKey
   *  @param authName Database column auth_name SqlType(varchar), Length(256,true), Default(None)
   *  @param authSrid Database column auth_srid SqlType(int4), Default(None)
   *  @param srtext Database column srtext SqlType(varchar), Length(2048,true), Default(None)
   *  @param proj4text Database column proj4text SqlType(varchar), Length(2048,true), Default(None) */
  case class SpatialRefSysRow(srid: Int, authName: Option[String] = None, authSrid: Option[Int] = None, srtext: Option[String] = None, proj4text: Option[String] = None)
  /** GetResult implicit for fetching SpatialRefSysRow objects using plain SQL queries */
  implicit def GetResultSpatialRefSysRow(implicit e0: GR[Int], e1: GR[Option[String]], e2: GR[Option[Int]]): GR[SpatialRefSysRow] = GR{
    prs => import prs._
    SpatialRefSysRow.tupled((<<[Int], <<?[String], <<?[Int], <<?[String], <<?[String]))
  }
  /** Table description of table spatial_ref_sys. Objects of this class serve as prototypes for rows in queries. */
  class SpatialRefSys(_tableTag: Tag) extends Table[SpatialRefSysRow](_tableTag, "spatial_ref_sys") {
    def * = (srid, authName, authSrid, srtext, proj4text) <> (SpatialRefSysRow.tupled, SpatialRefSysRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(srid), authName, authSrid, srtext, proj4text).shaped.<>({r=>import r._; _1.map(_=> SpatialRefSysRow.tupled((_1.get, _2, _3, _4, _5)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column srid SqlType(int4), PrimaryKey */
    val srid: Rep[Int] = column[Int]("srid", O.PrimaryKey)
    /** Database column auth_name SqlType(varchar), Length(256,true), Default(None) */
    val authName: Rep[Option[String]] = column[Option[String]]("auth_name", O.Length(256,varying=true), O.Default(None))
    /** Database column auth_srid SqlType(int4), Default(None) */
    val authSrid: Rep[Option[Int]] = column[Option[Int]]("auth_srid", O.Default(None))
    /** Database column srtext SqlType(varchar), Length(2048,true), Default(None) */
    val srtext: Rep[Option[String]] = column[Option[String]]("srtext", O.Length(2048,varying=true), O.Default(None))
    /** Database column proj4text SqlType(varchar), Length(2048,true), Default(None) */
    val proj4text: Rep[Option[String]] = column[Option[String]]("proj4text", O.Length(2048,varying=true), O.Default(None))
  }
  /** Collection-like TableQuery object for table SpatialRefSys */
  lazy val SpatialRefSys = new TableQuery(tag => new SpatialRefSys(tag))

  /** Entity class storing rows of table WifiKnownCredentials
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param username Database column username SqlType(text), Default(None)
   *  @param password Database column password SqlType(text)
   *  @param network Database column network SqlType(int4) */
  case class WifiKnownCredentialsRow(id: Int, username: Option[String] = None, password: String, network: Int)
  /** GetResult implicit for fetching WifiKnownCredentialsRow objects using plain SQL queries */
  implicit def GetResultWifiKnownCredentialsRow(implicit e0: GR[Int], e1: GR[Option[String]], e2: GR[String]): GR[WifiKnownCredentialsRow] = GR{
    prs => import prs._
    WifiKnownCredentialsRow.tupled((<<[Int], <<?[String], <<[String], <<[Int]))
  }
  /** Table description of table wifi_known_credentials. Objects of this class serve as prototypes for rows in queries. */
  class WifiKnownCredentials(_tableTag: Tag) extends Table[WifiKnownCredentialsRow](_tableTag, "wifi_known_credentials") {
    def * = (id, username, password, network) <> (WifiKnownCredentialsRow.tupled, WifiKnownCredentialsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), username, Rep.Some(password), Rep.Some(network)).shaped.<>({r=>import r._; _1.map(_=> WifiKnownCredentialsRow.tupled((_1.get, _2, _3.get, _4.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column username SqlType(text), Default(None) */
    val username: Rep[Option[String]] = column[Option[String]]("username", O.Default(None))
    /** Database column password SqlType(text) */
    val password: Rep[String] = column[String]("password")
    /** Database column network SqlType(int4) */
    val network: Rep[Int] = column[Int]("network")

    /** Foreign key referencing WifiNetwork (database name wifi_known_credentials_network_fkey) */
    lazy val wifiNetworkFk = foreignKey("wifi_known_credentials_network_fkey", network, WifiNetwork)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table WifiKnownCredentials */
  lazy val WifiKnownCredentials = new TableQuery(tag => new WifiKnownCredentials(tag))

  /** Entity class storing rows of table WifiNetwork
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param essid Database column essid SqlType(text) */
  case class WifiNetworkRow(id: Int, essid: String)
  /** GetResult implicit for fetching WifiNetworkRow objects using plain SQL queries */
  implicit def GetResultWifiNetworkRow(implicit e0: GR[Int], e1: GR[String]): GR[WifiNetworkRow] = GR{
    prs => import prs._
    WifiNetworkRow.tupled((<<[Int], <<[String]))
  }
  /** Table description of table wifi_network. Objects of this class serve as prototypes for rows in queries. */
  class WifiNetwork(_tableTag: Tag) extends Table[WifiNetworkRow](_tableTag, "wifi_network") {
    def * = (id, essid) <> (WifiNetworkRow.tupled, WifiNetworkRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(essid)).shaped.<>({r=>import r._; _1.map(_=> WifiNetworkRow.tupled((_1.get, _2.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column essid SqlType(text) */
    val essid: Rep[String] = column[String]("essid")

    /** Uniqueness Index over (essid) (database name wifi_network_essid_key) */
    val index1 = index("wifi_network_essid_key", essid, unique=true)
  }
  /** Collection-like TableQuery object for table WifiNetwork */
  lazy val WifiNetwork = new TableQuery(tag => new WifiNetwork(tag))

  /** Entity class storing rows of table WifiPollSpot
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param position Database column position SqlType(geometry), Length(2147483647,false)
   *  @param station Database column station SqlType(int4)
   *  @param time Database column time SqlType(timestamp)
   *  @param speed Database column speed SqlType(float8) */
  case class WifiPollSpotRow(id: Int, position: com.vividsolutions.jts.geom.Point, station: Int, time: java.sql.Timestamp, speed: Double)
  /** GetResult implicit for fetching WifiPollSpotRow objects using plain SQL queries */
  implicit def GetResultWifiPollSpotRow(implicit e0: GR[Int], e1: GR[com.vividsolutions.jts.geom.Point], e2: GR[java.sql.Timestamp], e3: GR[Double]): GR[WifiPollSpotRow] = GR{
    prs => import prs._
    WifiPollSpotRow.tupled((<<[Int], <<[com.vividsolutions.jts.geom.Point], <<[Int], <<[java.sql.Timestamp], <<[Double]))
  }
  /** Table description of table wifi_poll_spot. Objects of this class serve as prototypes for rows in queries. */
  class WifiPollSpot(_tableTag: Tag) extends Table[WifiPollSpotRow](_tableTag, "wifi_poll_spot") {
    def * = (id, position, station, time, speed) <> (WifiPollSpotRow.tupled, WifiPollSpotRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(position), Rep.Some(station), Rep.Some(time), Rep.Some(speed)).shaped.<>({r=>import r._; _1.map(_=> WifiPollSpotRow.tupled((_1.get, _2.get, _3.get, _4.get, _5.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column position SqlType(geometry), Length(2147483647,false) */
    val position: Rep[com.vividsolutions.jts.geom.Point] = column[com.vividsolutions.jts.geom.Point]("position", O.Length(2147483647,varying=false))
    /** Database column station SqlType(int4) */
    val station: Rep[Int] = column[Int]("station")
    /** Database column time SqlType(timestamp) */
    val time: Rep[java.sql.Timestamp] = column[java.sql.Timestamp]("time")
    /** Database column speed SqlType(float8) */
    val speed: Rep[Double] = column[Double]("speed")

    /** Foreign key referencing WifiStation (database name wifi_poll_spot_station_fkey) */
    lazy val wifiStationFk = foreignKey("wifi_poll_spot_station_fkey", station, WifiStation)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table WifiPollSpot */
  lazy val WifiPollSpot = new TableQuery(tag => new WifiPollSpot(tag))

  /** Entity class storing rows of table WifiStation
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param bssid Database column bssid SqlType(macaddr), Length(2147483647,false)
   *  @param network Database column network SqlType(int4)
   *  @param encryption Database column encryption SqlType(wifi_encryption_type) */
  case class WifiStationRow(id: Int, bssid: String, network: Int, encryption: String)
  /** GetResult implicit for fetching WifiStationRow objects using plain SQL queries */
  implicit def GetResultWifiStationRow(implicit e0: GR[Int], e1: GR[String]): GR[WifiStationRow] = GR{
    prs => import prs._
    WifiStationRow.tupled((<<[Int], <<[String], <<[Int], <<[String]))
  }
  /** Table description of table wifi_station. Objects of this class serve as prototypes for rows in queries. */
  class WifiStation(_tableTag: Tag) extends Table[WifiStationRow](_tableTag, "wifi_station") {
    def * = (id, bssid, network, encryption) <> (WifiStationRow.tupled, WifiStationRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(bssid), Rep.Some(network), Rep.Some(encryption)).shaped.<>({r=>import r._; _1.map(_=> WifiStationRow.tupled((_1.get, _2.get, _3.get, _4.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column bssid SqlType(macaddr), Length(2147483647,false) */
    val bssid: Rep[String] = column[String]("bssid", O.Length(2147483647,varying=false))
    /** Database column network SqlType(int4) */
    val network: Rep[Int] = column[Int]("network")
    /** Database column encryption SqlType(wifi_encryption_type) */
    val encryption: Rep[String] = column[String]("encryption")

    /** Foreign key referencing WifiNetwork (database name wifi_station_network_fkey) */
    lazy val wifiNetworkFk = foreignKey("wifi_station_network_fkey", network, WifiNetwork)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)

    /** Uniqueness Index over (bssid) (database name wifi_station_bssid_key) */
    val index1 = index("wifi_station_bssid_key", bssid, unique=true)
  }
  /** Collection-like TableQuery object for table WifiStation */
  lazy val WifiStation = new TableQuery(tag => new WifiStation(tag))
}
