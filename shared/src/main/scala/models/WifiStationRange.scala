package models

case class LatLng(lat: Double, lng: Double)

case class WifiStationRange(stationId: Int, bssid: String, essid: String, encryption: String, spots: Seq[LatLng])
