package utils

object HtmlUtils {
  def escape(str: String): String = {
    str.flatMap {
      case '<' => "&lt;"
      case '>' => "&gt;"
      case '"' => "&quot;"
      case '\'' => "&#x27;"
      case '&' => "&amp;"
      case x => String.valueOf(x)
    }
  }
}
