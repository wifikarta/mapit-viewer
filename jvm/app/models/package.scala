import play.api.db.slick.HasDatabaseConfigProvider

package object models {
  type HasDBConfigProvider = HasDatabaseConfigProvider[MyPgDriver]
}
