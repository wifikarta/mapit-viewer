package models

import com.vividsolutions.jts.geom.Point
import models.MyPgDriver.api._
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.{JsNumber, JsArray, JsString, JsObject}
import play.twirl.api.HtmlFormat

object LatLngs {
  def fromPoint(point: Point): LatLng = {
    LatLng(point.getX, point.getY)
  }
}

object WifiStationRanges {
  def all = {
    val q = for {
      station <- models.Tables.WifiStation
      network <- station.wifiNetworkFk
      if network.essid =!= "" // Hide networks with no public ESSID
      spot <- models.Tables.WifiPollSpot
      if spot.station === station.id
    } yield ((station.id, station.bssid, network.essid, station.encryption), spot.position)
    q.result
      .map(_.groupBy({ case (station, spot) => station })
      .map {
        case ((stationId, bssid, essid, encryption), spots) =>
          WifiStationRange(stationId, bssid, essid, encryption, spots.map(p => LatLngs.fromPoint(p._2)))
      })
  }
}
