package controllers

import javax.inject.Inject

import models.MyPgDriver.api._
import models._
import play.api.db.slick.DatabaseConfigProvider
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.mvc._
import upickle.default._

class Application @Inject()(protected val dbConfigProvider: DatabaseConfigProvider) extends Controller with HasDBConfigProvider {

  def index = Action {
    Ok(views.html.index())
  }

  def wifiPoints = Action.async(db.run {
    for {
      spots <- models.WifiStationRanges.all
    } yield {
      Ok(write(spots.toSeq))
    }
  })

  def routePath = Action.async(db.run {
    for {
      spots <- models.Tables.WifiPollSpot.result
    } yield {
      Ok(write(spots.map(s => (LatLngs.fromPoint(s.position), s.speed))))
    }
  })

}
