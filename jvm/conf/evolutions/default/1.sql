# --- !Ups

CREATE TYPE wifi_encryption_type AS ENUM ('NONE', 'WEP', 'WPA-PSK', 'WPA-MIXED-PSK', 'WPA2-PSK', 'WPA-802.1x', 'WPA-MIXED-802.1x', 'WPA2-802.1x');

CREATE TABLE wifi_network(
  id SERIAL PRIMARY KEY NOT NULL,
  essid TEXT NOT NULL UNIQUE
);

CREATE TABLE wifi_station(
  id SERIAL PRIMARY KEY NOT NULL,
  bssid MACADDR NOT NULL UNIQUE,
  network INTEGER NOT NULL REFERENCES wifi_network,
  encryption wifi_encryption_type NOT NULL
);

CREATE TABLE wifi_poll_spot (
  id SERIAL PRIMARY KEY NOT NULL,
  position GEOMETRY(POINT) NOT NULL,
  station INTEGER NOT NULL REFERENCES wifi_station,
  time TIMESTAMP NOT NULL,
  speed DOUBLE PRECISION NOT NULL
);

CREATE TABLE wifi_known_credentials (
  id SERIAL PRIMARY KEY NOT NULL,
  username TEXT,
  password TEXT NOT NULL,
  network INTEGER NOT NULL REFERENCES wifi_network
);

-- INSERT INTO wifi_network VALUES (1, 'Test');
-- INSERT INTO wifi_station VALUES (1, '14:da:e9:f1:e3:25', 1, 'NONE');
-- INSERT INTO wifi_poll_spot VALUES (1, 'POINT(59.471888 18.09256)', 1);
-- INSERT INTO wifi_poll_spot VALUES (2, 'POINT(59.481888 18.08256)', 1);
-- INSERT INTO wifi_poll_spot VALUES (5, 'POINT(59.471888 18.0725624)', 1);
--
-- INSERT INTO wifi_network VALUES (2, 'Test2!');
-- INSERT INTO wifi_station VALUES (2, '24:da:e9:f1:e3:25', 2, 'WPA2-PSK');
-- INSERT INTO wifi_poll_spot VALUES (3, 'POINT(59.571887 18.0925624)', 2);
-- INSERT INTO wifi_poll_spot VALUES (4, 'POINT(59.481888 18.0825624)', 2);
--
-- INSERT INTO wifi_network VALUES (3, 'eduroam');
-- INSERT INTO wifi_station VALUES (3, '35:da:e9:f1:e3:25', 3, 'WPA2-802.1x');
-- INSERT INTO wifi_poll_spot VALUES (6, 'POINT(59.451888 18.09256)', 3);
-- INSERT INTO wifi_poll_spot VALUES (7, 'POINT(59.461888 18.08256)', 3);
-- INSERT INTO wifi_poll_spot VALUES (8, 'POINT(59.451888 18.0725624)', 3);
-- INSERT INTO wifi_station VALUES (4, '45:da:e9:f1:e3:25', 3, 'WPA2-802.1x');
-- INSERT INTO wifi_poll_spot VALUES (9, 'POINT(59.441888 18.09256)', 4);
-- INSERT INTO wifi_poll_spot VALUES (10, 'POINT(59.451888 18.08256)', 4);
-- INSERT INTO wifi_poll_spot VALUES (11, 'POINT(59.441888 18.0725624)', 4);

# --- !Downs

DROP TABLE wifi_known_credentials;
DROP TABLE wifi_poll_spot;
DROP TABLE wifi_station;
DROP TABLE wifi_network;
DROP TYPE wifi_encryption_type;