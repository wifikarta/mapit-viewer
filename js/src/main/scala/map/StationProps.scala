package map

import scala.scalajs.js

@js.native
trait StationProps extends js.Object {
  def bssid: String = js.native
  def essid: String = js.native
  def encryption: String = js.native
  def popupContent: String = js.native
}
