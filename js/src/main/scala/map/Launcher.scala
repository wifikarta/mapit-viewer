package map

import leaflet._
import models.{LatLng, WifiStationRange}
import org.scalajs.dom.ext.{Ajax, AjaxException}
import upickle.default._
import utils.HtmlUtils

import scala.language.implicitConversions
import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue
import scala.scalajs.js
import scala.scalajs.js.JSApp
import scala.scalajs.js.annotation.JSExport

@JSExport
object Launcher extends JSApp {
  @JSExport
  override def main(): Unit = {
    val map = Leaflet.map("map").setView(new LeafletLatLng(59.471888, 18.0925624), 15)
    val stamenToner = TileLayer.provider("Stamen.Toner")
    val stamenWatercolor = TileLayer.provider("Stamen.Watercolor")
    stamenToner.addTo(map)

    val routeLayer = Leaflet.layerGroup()
    val wifiLayers = MapLayer.layers.toSeq.map(l => (l: MapLayer.Value, Leaflet.layerGroup()))

    val overlays = js.Dictionary[Layer](wifiLayers.map { case (k, v) => (k.toString, v) }: _*)
    overlays += "Route" -> routeLayer
    Control.layers(
      baseLayers = js.Dictionary[Layer]("Toner" -> stamenToner, "Watercolor" -> stamenWatercolor),
      overlays = overlays,
      options = new ControlOptions("topleft")
    ).addTo(map)
    wifiLayers.filter(_._1.showByDefault).foreach(_._2.addTo(map))
    loadMapEndpoint("/wifi-points.json", wifiLayers.toMap)
    loadRoutePath("/route.json", routeLayer)
  }

  def loadRoutePath(path: String, polylines: LayerGroup): Unit = {
    val f = Ajax.get(path)
    f.onFailure { case ex => ex.printStackTrace() }
    f.onSuccess { case xhr =>
      val points = read[Seq[(LatLng, Double)]](xhr.responseText)

      polylines.clearLayers()
      var prevPoint: Option[LatLng] = None
      for ((p, speed) <- points) {
        for (prev <- prevPoint) {
          val line = Leaflet.polyline(js.Array(LeafletLatLng(prev), LeafletLatLng(p)))
          line.bindPopup(s"Speed: $speed km/h")
          polylines.addLayer(line)
        }
        prevPoint = Some(p)
      }
      //polyline.setLatLngs(points.map(LeafletLatLng(_)).to[js.Array])
    }
  }

  def loadMapEndpoint(path: String, target: collection.immutable.Map[MapLayer.Value, LayerGroup]): Unit = {
    val f = Ajax.get(path)
    f.onFailure {
      case ex: AjaxException =>
        js.Dynamic.global.console.log("Failed to get wifi overlay", path, ex.xhr)
        ex.printStackTrace()
      case ex =>
        ex.printStackTrace()
    }
    f.onSuccess { case xhr =>
      val stations = read[Seq[WifiStationRange]](xhr.responseText)

      target.values.foreach(_.clearLayers())
      for (station <- stations) {
        val layer = MapLayer.pick(station)
        val polygon = Leaflet.polygon(
          latlngs = station.spots.map(LeafletLatLng.apply).to[js.Array],
          style = new Style(
            color = layer.color,
            weight = 5,
            opacity = 0.65
          )
        )
        polygon.bindPopup(
          s"""SSID: ${HtmlUtils.escape(station.essid)}
              |MAC: ${station.bssid}
              |Encryption: ${station.encryption}
          """.stripMargin.replace("\n", "<br/>"))
        target.get(layer).foreach(_.addLayer(polygon))
      }
    }
  }
}

object MapLayer extends Enumeration {

  class Value private[MapLayer](name: String, val color: String, val showByDefault: Boolean) extends Val(name)

  val Unencrypted = new Value("Unencrypted", "#ff0000", showByDefault = true)
  val Eduroam = new Value("Eduroam", "#00ff00", showByDefault = true)
  val Hotspot = new Value("Hotspot", "#ff0080", showByDefault = true)
  val EncryptedWEP = new Value("Encrypted (WEP, very weak)", "#ff00b0", showByDefault = false)
  val EncryptedHome = new Value("Encrypted (Home)", "#00ff40", showByDefault = false)
  val EncryptedCorporate = new Value("Encrypted (Corporate)", "#00ff80", showByDefault = false)
  val Portable = new Value("Portable (probably not very useful", "#000000", showByDefault = false)
  val Others = new Value("Others", "#ff7800", showByDefault = false)

  implicit def layerValue(value: super.Value): Value = value.asInstanceOf[Value]

  def layers = values - Others

  def pick(wifiStationRange: WifiStationRange): Value = wifiStationRange match {
    case WifiStationRange(_, _, "Homerun", "NONE", _) => MapLayer.Hotspot
    case WifiStationRange(_, _, "Espresso House", "NONE", _) => MapLayer.Hotspot
    case WifiStationRange(_, _, "MAX", "NONE", _) => MapLayer.Hotspot
    case WifiStationRange(_, _, "Bus4You", "NONE", _) => MapLayer.Portable
    case WifiStationRange(_, _, "AndroidAP", _, _) => MapLayer.Portable
    case WifiStationRange(_, _, IsIphone(_), _, _) => MapLayer.Portable
    case WifiStationRange(_, _, "eduroam", "WPA-MIXED-802.1x", _) => MapLayer.Eduroam
    case WifiStationRange(_, _, _, "NONE", _) => MapLayer.Unencrypted
    case WifiStationRange(_, _, _, "WEP", _) => MapLayer.EncryptedWEP
    case WifiStationRange(_, _, _, "WPA-PSK" | "WPA2-PSK" | "WPA-MIXED-PSK", _) => MapLayer.EncryptedHome
    case WifiStationRange(_, _, _, "WPA-802.1x" | "WPA2-802.1x" | "WPA-MIXED-802.1x", _) => MapLayer.EncryptedCorporate
    case _ =>
      js.Dynamic.global.console.warn(s"Network ${(wifiStationRange.bssid, wifiStationRange.essid)} did not match any layer, ignoring", js.JSON.parse(write(wifiStationRange)))
      MapLayer.Others
  }

  private object IsIphone {
    def unapply(name: String): Option[String] = {
      if (name.contains("iPhone"))
        Some(name)
      else
        None
    }
  }
}