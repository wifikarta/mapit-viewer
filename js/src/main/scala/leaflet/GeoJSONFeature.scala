package leaflet

import scala.scalajs.js

@js.native
trait GeoJSONFeature[Props] extends js.Object {
  def properties: Props = js.native
}
