package leaflet

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

@js.native
trait Layer extends js.Object with Addable

@js.native
trait HasPopup extends js.Object { this: Layer =>
  def bindPopup(body: String): Unit = js.native
}

@js.native
trait TileLayer extends Layer

@js.native
@JSName("L.tileLayer")
object TileLayer extends js.Object {
  def apply(url: String): TileLayer = js.native
  def provider(name: String): TileLayer = js.native
}

@js.native
trait FeatureLayer extends Layer with Styled with HasPopup

@js.native
trait PolylineLayer extends Layer with Styled with HasPopup {
  def setLatLngs(latlngs: js.Array[LeafletLatLng]): Unit = js.native
}

@js.native
trait PolygonLayer extends PolylineLayer

@js.native
trait LayerGroup extends Layer {
  def addLayer(layer: Layer): Unit = js.native
  def clearLayers(): Unit = js.native
}
