package leaflet

import scala.scalajs.js

@js.native
trait Map extends js.Object {
  def setView(pos: LeafletLatLng, zoom: Int): Map = js.native
}
