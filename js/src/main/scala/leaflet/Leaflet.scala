package leaflet

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

@js.native
@JSName("L")
object Leaflet extends js.Object {
  def map(name: String): Map = js.native
  def polyline(latlngs: js.Array[LeafletLatLng]): PolylineLayer = js.native
  def polygon(latlngs: js.Array[LeafletLatLng], style: js.UndefOr[Style] = js.undefined): PolygonLayer = js.native
  def layerGroup(layers: js.UndefOr[js.Array[Layer]] = js.undefined): LayerGroup = js.native
  def geoJson[Props](features: js.Array[GeoJSONFeature[Props]], options: GeoJSONOptions[Props]): Addable = js.native
}
