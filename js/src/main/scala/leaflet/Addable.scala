package leaflet

import scala.scalajs.js

@js.native
trait Addable extends js.Object {
  def addTo(map: Map): Unit = js.native
}
