package leaflet

import scala.scalajs.js
import scala.scalajs.js.annotation.{ScalaJSDefined, JSName}

@js.native
trait Control extends js.Object with Addable

@ScalaJSDefined
class ControlOptions(val position: String) extends js.Object

@js.native
@JSName("L.control")
object Control extends js.Object {
  def layers(baseLayers: js.UndefOr[js.Dictionary[Layer]] = js.undefined,
             overlays: js.UndefOr[js.Dictionary[Layer]] = js.undefined,
             options: js.UndefOr[ControlOptions] = js.undefined): Control = js.native
}
