package leaflet

import scala.scalajs.js
import scala.scalajs.js.annotation.ScalaJSDefined

@ScalaJSDefined
class Style(val color: js.UndefOr[String] = js.undefined,
            val weight: js.UndefOr[Int] = js.undefined,
            val opacity: js.UndefOr[Double] = js.undefined) extends js.Object

@js.native
trait Styled extends js.Object {
  def style: Style = js.native
  def setStyle(style: Style): Unit = js.native
}