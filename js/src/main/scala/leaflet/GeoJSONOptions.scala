package leaflet

import scala.scalajs.js
import scala.scalajs.js.annotation.ScalaJSDefined

@ScalaJSDefined
class GeoJSONOptions[Props](val style: js.UndefOr[Style] = js.undefined,
                            val onEachFeature: js.Function2[GeoJSONFeature[Props], Layer, Unit] = { (_: GeoJSONFeature[Props], _: Layer) => }) extends js.Object
