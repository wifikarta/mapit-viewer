package leaflet

import models.LatLng

import scala.scalajs.js
import scala.scalajs.js.annotation.ScalaJSDefined

@ScalaJSDefined
class LeafletLatLng(val lat: Double, val lng: Double) extends js.Object

object LeafletLatLng {
  def apply(latLng: LatLng) = new LeafletLatLng(latLng.lat, latLng.lng)
}
