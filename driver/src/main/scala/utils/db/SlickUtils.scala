package utils.db

import models.MyPgDriver
import models.MyPgDriver.api._
import slick.backend.DatabaseConfig

/**
  * Do *NOT* use this from Play, since it doesn't obey the Play config
  */
object SlickUtils {
  def getDBConfig: DatabaseConfig[MyPgDriver] = {
    val uri = this.getClass.getClassLoader.getResource("slick.conf").toURI
    val dc = DatabaseConfig.forURI[MyPgDriver](uri)
    dc
  }

  def getDB: Database = getDBConfig.db
}
