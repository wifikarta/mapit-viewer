package models

import slick.backend.DatabaseConfig
import slick.codegen.SourceCodeGenerator
import slick.driver.JdbcProfile
import slick.model.Model
import slick.profile.SqlProfile.ColumnOption.SqlType
import utils.db.SlickUtils

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}

class MySourceCodeGen(model: Model) extends SourceCodeGenerator(model) {

  // We need some implicits that the proper Profile doesn't provide
  override def packageCode(profile: String, pkg: String, container: String, parentType: Option[String]) : String = {
    s"""
package ${pkg}
// AUTO-GENERATED Slick data model
/** Stand-alone Slick data model for immediate use */
object ${container} extends {
  val profile = $profile
} with ${container}

/** Slick data model trait for extension, choice of backend or usage in the cake pattern. (Make sure to initialize this late.) */
trait ${container}${parentType.map(t => s" extends $t").getOrElse("")} {
  val profile: models.MyPgDriver
  import profile.api._
  ${indent(code)}
}
      """.trim()
  }

  // Currently jdbcTypeToScala is broken and doesn't actually pass the correct type to the driver, so we need to do it here...
  // See https://github.com/slick/slick/pull/1442
  override def Table: (slick.model.Table) => TableDef = new Table(_) {
    override def Column: (slick.model.Column) => ColumnDef = new Column(_) {
      override def rawType: String = {
        model.options.find {
          case tpe: SqlType => true
          case _ => false
        } match {
          case Some(SqlType("geometry")) => "com.vividsolutions.jts.geom.Point"
          case _ => super.rawType
        }
      }
    }
  }
}

// Adapted from [[slick.codegen.SourceCodeGenerator]]
object MySourceCodeGen {
  def main(args: Array[String]): Unit = {
    val dc = SlickUtils.getDBConfig
    val pkg = dc.config.getString("codegen.package")
    val out = args(0)
    val slickDriver = if(dc.driverIsObject) dc.driverName else "new " + dc.driverName
    try {
      val m = Await.result(dc.db.run(dc.driver.createModel(None, false)(ExecutionContext.global).withPinnedSession), Duration.Inf)
      new MySourceCodeGen(m).writeToFile(slickDriver, out, pkg)
    } finally dc.db.close
  }
}
