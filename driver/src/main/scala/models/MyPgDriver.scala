package models

import com.github.tminglei.slickpg._
import com.vividsolutions.jts.geom.Point

import scala.reflect.classTag

trait MyPgDriver extends ExPostgresDriver
  with PgArraySupport
  with PgPostGISSupport
  with PgDate2Support
  with PgNetSupport {

  bindPgTypeToScala("point", classTag[Point])

  override val api = MyAPI

  object MyAPI extends API with ArrayImplicits with PostGISImplicits with DateTimeImplicits with NetImplicits
}

object MyPgDriver extends MyPgDriver
