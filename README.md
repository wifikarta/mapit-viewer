# So you'd like to run your own instance of MapIt Viewer?

## Prerequisites

* PostgreSQL 9.5 (or newer): http://www.postgresql.org/
* PostGIS 2.2 (or newer): http://postgis.net/
* SBT launcher: http://www.scala-sbt.org/

## Setup

1. Create a postgres user with your computer login username. If you set a password then you must create a file in `driver/src/main/resources/local.slick.conf` containing `db.password = "PASSWORDGOESHERE"`.
2. Create a postgres database named mapit which is owned by the user you just created.
3. Enable PostGIS for the database
4. Run `$ sbt run` to launch the web server.
5. Go to http://localhost:9000/ and click confirm to set up the database.
6. Press `Ctrl+D` to exit back to the shell.
7. Run `$ sbt "importer/run importer/16-03-02-DiscImgSD.img"` to import the data. Don't worry if this fails with an exception, the dumps often end with a corrupted entry. The rest has been imported just fine.
8. Run `$ sbt run` to start up the server again.
9. Refresh your web browser.
10. There is no step 10 (yet)!